<?php

namespace Drupal\ajax_login_register_modal\Ajax;

use Drupal\Core\Ajax\CommandInterface;

/**
 * Provides a command to reload the page via AJAX.
 *
 * @package Drupal\ajax_login_register_modal\Ajax
 */
class ReloadCommand implements CommandInterface {

  /**
   * Return an array to be run through json_encode and sent to the client.
   */
  public function render() {
    return [
      'command' => 'reload',
    ];
  }

}
