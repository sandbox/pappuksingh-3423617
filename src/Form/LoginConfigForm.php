<?php

namespace Drupal\ajax_login_register_modal\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a configuration form for the Ajax Login and Register Modal.
 *
 * This form allows administrators to customize settings for user login modal,
 * including appearance, text, and redirect behavior.
 */
class LoginConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'ajax_login_register_modal.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'login_register_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('ajax_login_register_modal.settings');

    $form['login_form_settings']                                                        = [
      '#type' => 'details',
      '#title' => $this->t('User Login Modal & Form Settings Details'),
      '#open' => TRUE,
    ];
    $form['login_form_settings']['login_modal_title']                                   = [
      '#type' => 'textfield',
      '#title' => $this->t('Please choose the title of the modal window'),
      '#default_value' => $config->get('login_modal_title'),
    ];
    $form['login_form_settings']['user_login_form_disable_modal_title']                 = [
      '#type' => 'checkbox',
      '#title' => $this->t('Would you like to disable title for modal window?'),
      '#default_value' => $config->get('user_login_form_disable_modal_title'),
    ];
    $form['login_form_settings']['login_modal_width']                                   = [
      '#type' => 'textfield',
      '#title' => $this->t('Please choose width of modal window'),
      '#default_value' => $config->get('login_modal_width'),
      '#field_suffix'  => ' px',
    ];
    $form['login_form_settings']['login_modal_height']                                  = [
      '#type' => 'textfield',
      '#title' => $this->t('Please choose height of modal window'),
      '#default_value' => $config->get('login_modal_height'),
      '#field_suffix'  => ' px',
    ];
    $form['login_form_settings']['login_modal_drupal_auto_buttons']                     = [
      '#type' => 'checkbox',
      '#title' => $this->t('Would you like to disable auto buttons for modal window?'),
      '#default_value' => $config->get('login_modal_drupal_auto_buttons'),
    ];
    $form['login_form_settings']['user_login_form_action_button']                       = [
      '#type' => 'textfield',
      '#title' => $this->t('Please choose form action button text'),
      '#default_value' => $config->get('user_login_form_action_button'),
    ];
    $form['login_form_settings']['user_login_form_success_title']                       = [
      '#type' => 'textfield',
      '#title' => $this->t('Please choose login success title'),
      '#default_value' => $config->get('user_login_form_success_title'),
    ];
    $form['login_form_settings']['user_login_form_success_message']                     = [
      '#type' => 'textarea',
      '#title' => $this->t('Please choose login success message'),
      '#default_value' => $config->get('user_login_form_success_message'),
    ];
    $form['login_form_settings']['modal_links']                                         = [
      '#type'  => 'fieldset',
      '#title' => $this->t('Links settings'),
    ];
    $form['login_form_settings']['modal_links']['user_login_form_register_link']        = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Would you like to add register links for modal window?'),
      '#default_value' => $config->get('user_login_form_register_link'),
    ];
    $form['login_form_settings']['modal_links']['user_login_form_pass_link']            = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Would you like to add forgot password links for modal window?'),
      '#default_value' => $config->get('user_login_form_pass_link'),
    ];
    $form['login_form_settings']['modal_redirect']                                      = [
      '#type'  => 'fieldset',
      '#title' => $this->t('Redirect settings'),
    ];
    $options                                                                            = [
      'default' => $this->t('Default'),
      'custom'  => $this->t('Custom'),
      'refresh' => $this->t('Refresh'),
    ];
    $form['login_form_settings']['modal_redirect']['user_login_form_redirect_settings'] = [
      '#type'          => 'radios',
      '#options'       => $options,
      '#default_value' => $config->get('user_login_form_redirect_settings'),
      '#validated'     => TRUE,
    ];
    $form['login_form_settings']['modal_redirect']['user_login_form_redirect_url']      = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Custom redirect LINK'),
      '#description'   => $this->t('External and internal links. Examples: node/1, /node/1, http://example.com.'),
      '#default_value' => $config->get('user_login_form_redirect_url'),
      '#states'        => [
        'visible' => [
          ':input[name="user_login_form_redirect_settings"]' => ['value' => 'custom'],
        ],
      ],
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $values = $form_state->getValues();
    $config = $this->config('ajax_login_register_modal.settings');
    foreach ($values as $var => $value) {
      $config->set($var, $value)
        ->save();
    }
  }

}
