<?php

namespace Drupal\ajax_login_register_modal\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Manages the configuration form for the Ajax Login/Register block settings.
 *
 * This form allows administrators to enable or disable various elements of the
 * login/register block, customize button texts, and set the display style of
 * the links within the block.
 */
class BlockConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'ajax_login_register_modal.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'block_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('ajax_login_register_modal.settings');

    $form['login_register'] = [
      '#type' => 'details',
      '#title' => $this->t('Login/Regiter Block setting'),
      '#open' => TRUE,
    ];
    $form['login_register']['enabled_link'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Enabled Buttons'),
      '#options' => ajax_login_register_modal_get_links(),
      '#default_value' => $config->get('enabled_link') ? $config->get('enabled_link') : ['login', 'register', 'pass'],
    ];
    $options = [
      'vertical' => $this->t('Vertical'),
      'horizantol'  => $this->t('Horizantol'),
    ];
    $form['login_register']['links_display_style'] = [
      '#type' => 'radios',
      '#title' => $this->t('Liks Display Style'),
      '#options' => $options,
      '#default_value' => $config->get('links_display_style'),
    ];
    $modal_type_options = [
      'modal' => $this->t('Modal dialogs'),
      'dialog' => $this->t('Non modal dialogs'),
      'off_canvas'  => $this->t('Off canvas dialogs'),
    ];
    $form['login_register']['ajax_modal_type'] = [
      '#type'          => 'radios',
      '#title' => $this->t('Please choose display modal type'),
      '#options'       => $modal_type_options,
      '#default_value' => $config->get('ajax_modal_type'),
      '#validated'     => TRUE,
    ];
    $form['login_register']['login_button'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Login Link Text'),
      '#default_value' => $config->get('login_button'),
      '#description' => $this->t("Add the link text which will display on Popup Block for login"),
    ];
    $form['login_register']['register_button'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Register Link Text'),
      '#default_value' => $config->get('register_button'),
      '#description' => $this->t("Add the link text which will display on Popup Block for Register"),
    ];
    $form['login_register']['pass_button'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Rest Password Link Text'),
      '#default_value' => $config->get('pass_button'),
      '#description' => $this->t("Add the link text which will display on Popup Block for Reset Password"),
    ];
    $form['login_register']['user_login_form_progress_message'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Login Progress Message'),
      '#default_value' => $config->get('login_progress_message'),
      '#description' => $this->t("Add the Login progress message"),
    ];
    $form['login_register']['user_register_form_progress_message'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Register Progress Message'),
      '#default_value' => $config->get('register_progress_message'),
      '#description' => $this->t("Add the progress message"),
    ];
    $form['login_register']['user_pass_progress_message'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Rest Password Progress Message'),
      '#default_value' => $config->get('user_pass_progress_message'),
      '#description' => $this->t("Add rest password progress message"),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $values = $form_state->getValues();
    $config = $this->config('ajax_login_register_modal.settings');
    foreach ($values as $var => $value) {
      $config->set($var, $value)
        ->save();
    }
  }

}
