<?php

namespace Drupal\ajax_login_register_modal\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class RegisterConfigForm.
 *
 * This form allows administrators to configure the settings for the
 * registration modal and form, such as titles, dimensions,and redirect options.
 *
 * @package Drupal\LoginRegisterConfigForm\Form
 */
class RegisterConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'ajax_login_register_modal.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'login_register_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config                                                                         = $this->config('ajax_login_register_modal.settings');
    $form['register_form_settings']                                                 = [
      '#type' => 'details',
      '#title' => $this->t('Register Modal & Form Settings Details'),
      '#open' => TRUE,
    ];
    $form['register_form_settings']['register_modal_title']                         = [
      '#type' => 'textfield',
      '#title' => $this->t('Please choose title of modal window'),
      '#default_value' => $config->get('register_modal_title'),
      '#description' => $this->t("Add register the modal title"),
    ];
    $form['register_form_settings']['user_register_form_disable_modal_title']       = [
      '#type' => 'checkbox',
      '#title' => $this->t('Would you like to disable title for modal window?'),
      '#default_value' => $config->get('user_register_form_disable_modal_title'),
    ];
    $form['register_form_settings']['register_modal_width']                         = [
      '#type' => 'textfield',
      '#title' => $this->t('Please choose width of modal window'),
      '#default_value' => $config->get('register_modal_width'),
      '#field_suffix'  => ' px',
      '#description' => $this->t("Add the modal width like Expmples: 800"),
    ];
    $form['register_form_settings']['register_modal_height']                        = [
      '#type' => 'textfield',
      '#title' => $this->t('Please choose height of modal window'),
      '#default_value' => $config->get('register_modal_height'),
      '#field_suffix'  => ' px',
      '#description' => $this->t("Add the modal height like Expmples: 400"),
    ];
    $form['register_form_settings']['register_modal_drupal_auto_buttons']           = [
      '#type' => 'checkbox',
      '#title' => $this->t('Would you like to disable auto buttons for modal window?'),
      '#default_value' => $config->get('register_modal_drupal_auto_buttons'),
    ];
    $form['register_form_settings']['user_register_form_action_button']             = [
      '#type' => 'textfield',
      '#title' => $this->t('Please choose form action button text'),
      '#default_value' => $config->get('user_register_form_action_button'),
      '#description' => $this->t("Register Form Submit Button Text which will display in Popup Register Form"),
    ];
    $form['register_form_settings']['user_register_form_success_title']             = [
      '#type' => 'textfield',
      '#title' => $this->t('Please choose register success title'),
      '#default_value' => $config->get('user_register_form_success_title'),
    ];
    $form['register_form_settings']['user_register_form_success_message']           = [
      '#type' => 'textarea',
      '#title' => $this->t('Please choose register success message'),
      '#default_value' => $config->get('user_register_form_success_message'),
    ];
    $form['register_form_settings']['modal_links']                                  = [
      '#type'  => 'fieldset',
      '#title' => $this->t('Links settings'),
    ];
    $form['register_form_settings']['modal_links']['user_register_form_login_link'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Would you like to add login links for modal window?'),
      '#default_value' => $config->get('user_register_form_login_link'),
    ];

    $form['register_form_settings']['modal_links']['user_register_form_pass_link']            = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Would you like to add reset password links for modal window?'),
      '#default_value' => $config->get('user_register_form_pass_link'),
    ];
    $form['register_form_settings']['modal_redirect']                                         = [
      '#type'  => 'fieldset',
      '#title' => $this->t('Redirect settings'),
    ];
    $options                                                                                  = [
      'default' => $this->t('Default'),
      'custom'  => $this->t('Custom'),
      'refresh' => $this->t('Refresh'),
    ];
    $form['register_form_settings']['modal_redirect']['user_register_form_redirect_settings'] = [
      '#type'          => 'radios',
      '#options'       => $options,
      '#default_value' => $config->get('user_register_form_redirect_settings'),
      '#validated'     => TRUE,
    ];
    $form['register_form_settings']['modal_redirect']['user_register_form_redirect_url']      = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Custom redirect link'),
      '#description'   => $this->t('External and internal links. Examples: node/1, /node/1, http://example.com.'),
      '#default_value' => $config->get('user_register_form_redirect_url'),
      '#states'        => [
        'visible' => [
          ':input[name="user_register_form_redirect_settings"]' => ['value' => 'custom'],
        ],
      ],
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $values = $form_state->getValues();
    $config = $this->config('ajax_login_register_modal.settings');
    foreach ($values as $var => $value) {
      $config->set($var, $value)
        ->save();
    }
  }

  /**
   * Checks if the login link is enabled in the form settings.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state interface.
   *
   * @return mixed
   *   Returns the login wrapper settings.
   */
  public function checkLoginLinkEnabled(array $form, FormStateInterface $form_state) {
    return $form['register_form_settings']['login_wrapper'];
  }

  /**
   * Checks if the register link is enabled in the form settings.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state interface.
   *
   * @return mixed
   *   Returns the register wrapper settings.
   */
  public function checkRegisterLinkEnabled(array $form, FormStateInterface $form_state) {
    return $form['login_form_settings']['register_wrapper'];
  }

  /**
   * Checks if the forgot password link is enabled in the form settings.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state interface.
   *
   * @return mixed
   *   Returns the forgot password wrapper settings.
   */
  public function checkForgotpwdLinkEnabled(array $form, FormStateInterface $form_state) {
    return $form['register_form_settings']['forgotpwd_wrapper'];
  }

}
