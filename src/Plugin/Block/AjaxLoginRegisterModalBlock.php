<?php

namespace Drupal\ajax_login_register_modal\Plugin\Block;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Link;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountProxy;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'AjaxLoginRegisterModalBlock' block.
 *
 * @Block(
 *  id = "ajax_login_register_modal_block",
 *  admin_label = @Translation("Ajax Login, Register, Password Reset Modal Links Block"),
 * )
 */
class AjaxLoginRegisterModalBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Info about current user.
   *
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected $currentUser;

  /**
   * Config of the module.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $config_factory, AccountProxy $current_user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->currentUser = $current_user;
    $this->config = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->config->get('ajax_login_register_modal.settings');
    if (!$this->currentUser->isAnonymous()) {
      return;
    }
    $enabled_links = $config->get('enabled_link');
    $links_display_style = $config->get('links_display_style');
    $build = [];
    foreach ($enabled_links as $enabled_link) {
      if ($enabled_link == 0) {
        continue;
      }
      $url = Url::fromRoute('user.' . $enabled_link);
      // $link_text = $config->get($enabled_link . '_button');
      // $modal_option = getModalOptions($enabled_link);
      $modal_type = $config->get('ajax_modal_type');
      $class = ['use-ajax', $enabled_link . '-popup-form', 'nav-link'];
      $attributes = [];
      $attributes['class'] = $class;
      if ($modal_type == 'off_canvas') {
        $attributes['data-dialog-type'] = 'dialog';
        $attributes['data-dialog-renderer'] = $modal_type;
      }
      else {
        $attributes['data-dialog-type'] = $modal_type;
      }
      $modal_option = [];
      $modal_option['title'] = $config->get($enabled_link . '_modal_title');
      $modal_option['width'] = $config->get($enabled_link . '_modal_width');
      $modal_option['height'] = $config->get($enabled_link . '_modal_height');
      $modal_option['drupalAutoButtons'] = $config->get($enabled_link . '_modal_drupal_auto_buttons') ? FALSE : TRUE;
      $attributes['data-dialog-options'] = Json::encode($modal_option);

      $link_options = [
        'attributes' => $attributes,
      ];
      $url->setOptions($link_options);
      $link = Link::fromTextAndUrl($this->t('Link Text Here'), $url)->toString();
      if ($links_display_style == 'vertical') {
        $build['block'][$enabled_link]['#markup'] = '<div class="link-item">' . $link . '</div>';
      }
      else {
        $build['block'][$enabled_link]['#markup'] = $link;
      }
    }
    $build['block']['#attached']['library'][] = 'core/drupal.dialog.ajax';
    return $build;
  }

}
