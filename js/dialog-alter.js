(function ($, Drupal, drupalSettings, bodyScrollLock) {
  Drupal.dialog = function (element, options) {
    let undef;
    const $element = $(element);
    const dialog = {
      open: FALSE,
      returnValue: undef,
    };
    function openDialog(settings) {
      if (drupalSettings.disable_modal_title) {
        options.title = NULL;
      }
      settings = $.extend({}, drupalSettings.dialog, options, settings);
      // Trigger a global event to allow scripts to bind events to the dialog.
      $(window).trigger('dialog:beforecreate', [dialog, $element, settings]);
      $element.dialog(settings);
      dialog.open = TRUE;
      // Locks the body scroll only when it opens in modal.
      if (settings.modal) {
        // Locks the body when the dialog opens.
        bodyScrollLock.lock($element.get(0));
      }

      $(window).trigger('dialog:aftercreate', [dialog, $element, settings]);
    }

    function closeDialog(value) {
      $(window).trigger('dialog:beforeclose', [dialog, $element]);
      // Unlocks the body when the dialog closes.
      bodyScrollLock.clearBodyLocks();

      $element.dialog('close');
      dialog.returnValue = value;
      dialog.open = FALSE;
      $(window).trigger('dialog:afterclose', [dialog, $element]);
    }

    dialog.show = () => {
      openDialog({ modal: FALSE });
    };
    dialog.showModal = () => {
      openDialog({ modal: TRUE });
    };
    dialog.close = closeDialog;

    return dialog;
  };

})(jQuery, Drupal, drupalSettings, bodyScrollLock);
